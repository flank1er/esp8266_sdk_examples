#include "ets_sys.h"
#include "user_interface.h"
#include "gpio.h"
#include "driver/gpio16.h"

void  dummy_loop(uint32_t count ){
	while(--count) {
	}
}

void ICACHE_FLASH_ATTR user_init()
{
	gpio_init();

	// map GPIO16 as push-pull  pin
	gpio16_output_conf();

	for(;;){
		dummy_loop(600000);
		system_soft_wdt_stop();
		gpio16_output_set(0x1);		// High
		dummy_loop(600000);
		system_soft_wdt_feed();
		gpio16_output_set(0x0);		// Low
	}
}
