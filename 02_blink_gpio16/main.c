#include "ets_sys.h"
#include "user_interface.h"
#include "gpio.h"

#define LED 16


void  dummy_loop(uint32_t count ){
	while(--count) {
	}
}

void ICACHE_FLASH_ATTR user_init()
{
	gpio_init();

	// map GPIO16 as an I/O pin
	// https://bbs.espressif.com/viewtopic.php?t=1521
	uint32_t pinHigh;
	uint32_t val = READ_PERI_REG(PAD_XPD_DCDC_CONF) & 0xffffffbc;
	WRITE_PERI_REG(PAD_XPD_DCDC_CONF, val | 0x00000001);	//mux configuration for XPD_DCDC to output rtc_gpio0
	val = READ_PERI_REG(RTC_GPIO_CONF) & 0xfffffffe;
	WRITE_PERI_REG(RTC_GPIO_CONF, val | 0x00000000);		// mux configuration for out enable
	//  push-pull mode
    val = READ_PERI_REG(RTC_GPIO_ENABLE);
    WRITE_PERI_REG(RTC_GPIO_ENABLE, val | (uint32_t)0x01);
	val = READ_PERI_REG(RTC_GPIO_OUT);

	for(;;){
		dummy_loop(600000);
		system_soft_wdt_stop();
		WRITE_PERI_REG(RTC_GPIO_OUT, (val | (uint32_t)0x01)); // high level 
		dummy_loop(600000);
		system_soft_wdt_feed();
        WRITE_PERI_REG(RTC_GPIO_OUT, (val & ~(uint32_t)0x01)); // low level
	}
}
