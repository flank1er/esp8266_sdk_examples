#include "ets_sys.h"
#include "user_interface.h"
#include "gpio.h"

#define LED 2


void  dummy_loop(uint32_t count ){
	while(--count) {
	}
}

void ICACHE_FLASH_ATTR user_init()
{
	// init gpio subsytem
	gpio_init();

	// configure UART TXD to be GPIO1, set as output
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0TXD_U, FUNC_GPIO1);
	gpio_output_set(0, 0, (1 << LED), 0);
	for(;;){
		dummy_loop(600000);
		system_soft_wdt_stop();
        gpio_output_set(0, (1 << LED), 0, 0);	// led off
		dummy_loop(600000);
		system_soft_wdt_feed();
        gpio_output_set((1 << LED), 0, 0, 0);   // led on
	}
}
