#include "ets_sys.h"
#include "user_interface.h"
#include "osapi.h"
#include "gpio.h"
#include "driver/uart.h"
#include "espmissingincludes.h"
#include "espconn.h"
#include "mem.h"
#include "ping.h"
#include "inc/user_config.h"

#define http_port 8010
#define ip_addr "192.168.1.10" // Alien

static struct station_config wifi_config;
struct ip_info ipConfig;
char buffer[512];

struct espconn ping_conn;
char ping_host[] = "ya.ru";
ip_addr_t ping_ip;

void print_connect_status();
void wifi_handle_event_cb(System_Event_t *evt);
static void ICACHE_FLASH_ATTR tcp_connected(void *arg);
static void ICACHE_FLASH_ATTR data_received_cb(void *arg, char *pdata, unsigned short len);
static void ICACHE_FLASH_ATTR tcp_disconnected_cb(void *arg);
void ICACHE_FLASH_ATTR user_test_ping(ip_addr_t *ipaddr);

void ICACHE_FLASH_ATTR dns_done(const char *name, ip_addr_t *ipaddr, void *arg )
{
    struct espconn *conn = (struct espconn *)arg;

	espconn_disconnect(conn);

    os_printf( "%s\n", __FUNCTION__ );

    if (ipaddr != NULL)
		user_test_ping(ipaddr);
   else
		os_printf("DNS lookup failed\n");

}

void ICACHE_FLASH_ATTR user_ping_recv(void *arg, void *pdata) {
    struct ping_resp *ping_resp = pdata;
    struct ping_option *ping_opt = arg;

    if (ping_resp->ping_err == -1)
        os_printf("ping host fail \r\n");
    else
        os_printf("ping recv: byte = %d, time = %d ms \r\n",ping_resp->bytes,ping_resp->resp_time);

}

void ICACHE_FLASH_ATTR user_ping_sent(void *arg, void *pdata)
{
    os_printf("user ping finish \r\n");
    wifi_station_disconnect();
}

void ICACHE_FLASH_ATTR user_test_ping(ip_addr_t *ipaddr) {
    struct ping_option *ping_opt = NULL;
//    const char* ping_ip = "8.8.8.8";

    ping_opt = (struct ping_option *)os_zalloc(sizeof(struct ping_option));

    os_printf("ping to: " IPSTR, IP2STR(&ipaddr->addr));
    os_printf("\n");

    ping_opt->count = 10;    //  try to ping how many times
    ping_opt->coarse_time = 2;  // ping interval
//    ping_opt->ip = ipaddr_addr(ping_ip);
    ping_opt->ip = ipaddr->addr;

    ping_regist_recv(ping_opt,user_ping_recv);
    ping_regist_sent(ping_opt,user_ping_sent);

    ping_start(ping_opt);

}

static void ICACHE_FLASH_ATTR send_data() {
    uint32 ip=0;
    struct espconn *conn = (struct espconn *)os_zalloc(sizeof(struct espconn));
    if (conn != NULL) {
        conn->type = ESPCONN_TCP;
        conn->state = ESPCONN_NONE;
        conn->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
        conn->proto.tcp->local_port = espconn_port();
        conn->proto.tcp->remote_port = http_port;
        ip = ipaddr_addr(ip_addr);
        os_memcpy(conn->proto.tcp->remote_ip,&ip,sizeof(ip));
        espconn_regist_connectcb(conn, tcp_connected);
        espconn_regist_disconcb(conn, tcp_disconnected_cb);
        espconn_regist_recvcb(conn, data_received_cb);
        espconn_connect(conn);
    } else
        uart0_sendStr("TCP connect failed!\r\n");
}


/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR user_init(void)
{
	// UART config
	uart_init(BIT_RATE_115200, BIT_RATE_115200);

    os_printf("SDK version:%s\n", system_get_sdk_version());

    // wifi connect
    wifi_set_event_handler_cb(wifi_handle_event_cb);

    if (wifi_set_opmode(STATION_MODE)) {
	    wifi_station_disconnect();
	    os_memcpy(wifi_config.ssid, SSID, sizeof(SSID));
    	os_memcpy(wifi_config.password, PASSWORD, sizeof(PASSWORD));
	    wifi_config.bssid_set=0;

    	wifi_station_set_config(&wifi_config);
	} else
		uart0_sendStr("ERROR: setting the station mode has failed.\n");

}

void ICACHE_FLASH_ATTR
user_rf_pre_init(void)
{
}

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        case FLASH_SIZE_64M_MAP_1024_1024:
            rf_cal_sec = 2048 - 5;
            break;
        case FLASH_SIZE_128M_MAP_1024_1024:
            rf_cal_sec = 4096 - 5;
            break;
        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}


void ICACHE_FLASH_ATTR  print_connect_status() {
    uint8_t status=wifi_station_get_connect_status();
    switch (status) {
    case STATION_IDLE :
        uart0_sendStr("\nCurrent status is: Idle\n");
        break;
    case STATION_CONNECTING :
        uart0_sendStr("\nCurrent status is: Connecting\n");
        break;
    case STATION_WRONG_PASSWORD :
        uart0_sendStr("\nCurrent status is: Wrong password\n");
        break;
    case STATION_NO_AP_FOUND :
        uart0_sendStr("\nCurrent status is: No AP found\n");
        break;
    case STATION_CONNECT_FAIL :
        uart0_sendStr("\nCurrent status is: Connect Fail\n");
        break;
    case STATION_GOT_IP :
        uart0_sendStr("\nCurrent status is: Got IP\n");
        break;
     default:
        os_printf("Current status is: %d\n",status);
    }

    if ((status == STATION_GOT_IP) &&  wifi_get_ip_info(STATION_IF, &ipConfig)) {
        os_printf("ip: " IPSTR, IP2STR(&ipConfig.ip.addr));
    }
}

void ICACHE_FLASH_ATTR  wifi_handle_event_cb(System_Event_t *evt) {
    os_printf("event %x: ", evt->event);
    switch  (evt->event)    {
    case    EVENT_STAMODE_CONNECTED:
		uart0_sendStr("EVENT_STAMODE_CONNECTED\n");
        os_printf("connect to ssid %s, channel %d\n",
            evt->event_info.connected.ssid,
            evt->event_info.connected.channel);
        break;
    case    EVENT_STAMODE_DISCONNECTED:
		uart0_sendStr("EVENT_STAMODE_DISCONNECTED\n");
        os_printf("disconnect from ssid %s, reason %d\n",
            evt->event_info.disconnected.ssid,
            evt->event_info.disconnected.reason);

		system_deep_sleep_instant(60000*1000);		// 60 sec
		system_deep_sleep_set_option(2);

        break;
    case    EVENT_STAMODE_AUTHMODE_CHANGE:
		uart0_sendStr("EVENT_STAMODE_AUTHMODE_CHANGE\n");
        os_printf("mode: %d -> %d\n",
            evt->event_info.auth_change.old_mode,
            evt->event_info.auth_change.new_mode);
        break;
    case EVENT_STAMODE_GOT_IP:
		uart0_sendStr("EVENT_STAMODE_GOT_IP\n");
        os_printf("ip:" IPSTR ",mask:" IPSTR ",gw:" IPSTR,
            IP2STR(&evt->event_info.got_ip.ip),
            IP2STR(&evt->event_info.got_ip.mask),
            IP2STR(&evt->event_info.got_ip.gw));
        os_printf("\n");
		espconn_gethostbyname(&ping_conn, ping_host, &ping_ip, dns_done);
//		user_test_ping();
//		send_data();
        break;
    case EVENT_SOFTAPMODE_STACONNECTED:
		uart0_sendStr("EVENT_SOFTAPMODE_STACONNECTED\n");
        os_printf("station: " MACSTR "join, AID = %d\n",
            MAC2STR(evt->event_info.sta_connected.mac),
            evt->event_info.sta_connected.aid);
        break;
    case EVENT_SOFTAPMODE_STADISCONNECTED:
		uart0_sendStr("EVENT_SOFTAPMODE_STADISCONNECTED\n");
        os_printf("station: " MACSTR "leave, AID = %d\n",
            MAC2STR(evt->event_info.sta_disconnected.mac),
            evt->event_info.sta_disconnected.aid);
        break;
	default:
        break;
     }
}

static void ICACHE_FLASH_ATTR  data_received_cb(void *arg, char *pdata, unsigned short len )
{
    struct espconn *conn = (struct espconn *)arg;
    os_printf( "%s: %s\n", __FUNCTION__, pdata);
}

static void ICACHE_FLASH_ATTR tcp_disconnected_cb(void *arg)
{
    struct espconn *conn = (struct espconn *)arg;

    os_printf( "%s\n", __FUNCTION__ );
    os_printf("esp8266 disconnected\r\n");
    wifi_station_disconnect();
}

static void ICACHE_FLASH_ATTR tcp_connected(void *arg)
{
    struct espconn *conn = arg;

    os_printf( "%s\n", __FUNCTION__ );
    os_sprintf( buffer, "GET / HTTP/1.1\r\n\r\n");

    os_printf("Sending: %s\n", buffer);
    espconn_sent(conn, buffer, os_strlen(buffer));
}

