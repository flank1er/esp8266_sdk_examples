#include "ets_sys.h"
#include "user_interface.h"
#include "gpio.h"
#include "osapi.h"
#include "driver/gpio16.h"
#include "driver/uart.h"
#include "espmissingincludes.h"

//void ets_isr_mask(unsigned intr);
//void ets_isr_unmask(unsigned intr);

void  dummy_loop(uint32_t count ){
	while(--count) {
	}
}

void ICACHE_FLASH_ATTR user_init()
{
	gpio_init();
	// map GPIO16 as push-pull  pin
	gpio16_output_conf();
	// UART config
	uart_init(BIT_RATE_115200, BIT_RATE_115200);
	// let's go... 
	system_set_os_print(0x01);

	for(;;){
		dummy_loop(6000000);
		system_soft_wdt_stop();
		gpio16_output_set(0x1);		// High
		dummy_loop(6000000);
		system_soft_wdt_feed();
		gpio16_output_set(0x0);		// Low
//		uart0_sendStr("Test ESP8266\r\n");
		os_printf("SDK version: %s\n", system_get_sdk_version());
		os_printf("Chip ID: 0x%x\n", system_get_chip_id());
		os_printf("--- meminfo----\n");
		system_print_meminfo();
		os_printf("Heap size: %u\n", system_get_free_heap_size());
		os_printf("CPU frequency: %u\n", system_get_cpu_freq());
		os_printf("System time: %ums\n", system_get_time());
		os_printf("RTC time: %u\n", system_get_rtc_time());
		os_printf("Boot version: %u\n", system_get_boot_version());
		os_printf("Userbin address: %u\n", system_get_userbin_addr());
		os_printf("Boot mode: %u\n", system_get_boot_mode());
	}
}
