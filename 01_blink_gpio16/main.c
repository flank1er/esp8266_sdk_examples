#include "ets_sys.h"
#include "user_interface.h"
#include "gpio.h"

void  dummy_loop(uint32_t count ){
	while(--count) {
	}
}

void ICACHE_FLASH_ATTR user_init()
{
	// init gpio subsytem
	gpio_init();

    // https://bbs.espressif.com/viewtopic.php?t=1521
    // map GPIO16 as an I/O pin
    uint32_t val = READ_PERI_REG(PAD_XPD_DCDC_CONF) & 0xffffffbc;
    WRITE_PERI_REG(PAD_XPD_DCDC_CONF, val | 0x00000001);    //mux configuration for XPD_DCDC to output rtc_gpio0
    val = READ_PERI_REG(RTC_GPIO_CONF) & 0xfffffffe;
    WRITE_PERI_REG(RTC_GPIO_CONF, val | 0x00000000);        // mux configuration for out enable
 
	for(;;){
		dummy_loop(600000);
		system_soft_wdt_stop();
        SET_PERI_REG_MASK(RTC_GPIO_ENABLE, (uint32_t)0x01);
		dummy_loop(600000);
		system_soft_wdt_feed();
        CLEAR_PERI_REG_MASK(RTC_GPIO_ENABLE, (uint32_t)0x01);
	}
}
