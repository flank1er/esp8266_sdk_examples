#include "ets_sys.h"
#include "user_interface.h"
#include "osapi.h"
#include "gpio.h"
#include "driver/gpio16.h"
#include "driver/uart.h"
#include "espmissingincludes.h"
#include "inc/user_config.h"

static struct station_config wifi_config;
struct ip_info ipConfig;
static os_timer_t gpio16_timer;
uint8_t led_status;

void print_connect_status();
void wifi_handle_event_cb(System_Event_t *evt);

/******************************************************************************
 * FunctionName : blink
 * Description  : toggle gpio16
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
LOCAL void blink(void *arg) {
	if (led_status)
		gpio16_output_set(0x1);		// High
	else
		gpio16_output_set(0x0);		// Low

	led_status= !led_status;
//	print_connect_status();
}

/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR user_init(void)
{
	// init varialbles
	led_status=0;
	// inti periphery
	gpio_init();
	// map GPIO16 as push-pull  pin
	gpio16_output_conf();
	// UART config
	uart_init(BIT_RATE_115200, BIT_RATE_115200);

	// blink timer (1000ms, repeating)
	os_timer_setfn(&gpio16_timer, (os_timer_func_t *)blink, NULL);
	os_timer_arm(&gpio16_timer, 1000, 1);

    os_printf("SDK version:%s\n", system_get_sdk_version());

    // wifi connect
    wifi_set_event_handler_cb(wifi_handle_event_cb);

    if (wifi_set_opmode(STATION_MODE)) {
	    wifi_station_disconnect();
	    os_memcpy(wifi_config.ssid, SSID, sizeof(SSID));
    	os_memcpy(wifi_config.password, PASSWORD, sizeof(PASSWORD));
	    wifi_config.bssid_set=0;

    	wifi_station_set_config(&wifi_config);
	} else
		uart0_sendStr("ERROR: setting the station mode has failed.\n");
}

void ICACHE_FLASH_ATTR
user_rf_pre_init(void)
{
}

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
uint32 ICACHE_FLASH_ATTR
user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        case FLASH_SIZE_64M_MAP_1024_1024:
            rf_cal_sec = 2048 - 5;
            break;
        case FLASH_SIZE_128M_MAP_1024_1024:
            rf_cal_sec = 4096 - 5;
            break;
        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}


void print_connect_status() {
    uint8_t status=wifi_station_get_connect_status();
    switch (status) {
    case STATION_IDLE :
        uart0_sendStr("\nCurrent status is: Idle\n");
        break;
    case STATION_CONNECTING :
        uart0_sendStr("\nCurrent status is: Connecting\n");
        break;
    case STATION_WRONG_PASSWORD :
        uart0_sendStr("\nCurrent status is: Wrong password\n");
        break;
    case STATION_NO_AP_FOUND :
        uart0_sendStr("\nCurrent status is: No AP found\n");
        break;
    case STATION_CONNECT_FAIL :
        uart0_sendStr("\nCurrent status is: Connect Fail\n");
        break;
    case STATION_GOT_IP :
        uart0_sendStr("\nCurrent status is: Got IP\n");
        break;
     default:
        os_printf("Current status is: %d\n",status);
    }

    if ((status == STATION_GOT_IP) &&  wifi_get_ip_info(STATION_IF, &ipConfig)) {
        os_printf("ip: " IPSTR, IP2STR(&ipConfig.ip.addr));
    }
}

void wifi_handle_event_cb(System_Event_t *evt) {
    os_printf("event %x: ", evt->event);
    switch  (evt->event)    {
    case    EVENT_STAMODE_CONNECTED:
		uart0_sendStr("EVENT_STAMODE_CONNECTED\n");
        os_printf("connect to ssid %s, channel %d\n",
            evt->event_info.connected.ssid,
            evt->event_info.connected.channel);
        break;
    case    EVENT_STAMODE_DISCONNECTED:
		uart0_sendStr("EVENT_STAMODE_DISCONNECTED\n");
        os_printf("disconnect from ssid %s, reason %d\n",
            evt->event_info.disconnected.ssid,
            evt->event_info.disconnected.reason);
        break;
    case    EVENT_STAMODE_AUTHMODE_CHANGE:
		uart0_sendStr("EVENT_STAMODE_AUTHMODE_CHANGE\n");
        os_printf("mode: %d -> %d\n",
            evt->event_info.auth_change.old_mode,
            evt->event_info.auth_change.new_mode);
        break;
    case EVENT_STAMODE_GOT_IP:
		uart0_sendStr("EVENT_STAMODE_GOT_IP\n");
        os_printf("ip:" IPSTR ",mask:" IPSTR ",gw:" IPSTR,
            IP2STR(&evt->event_info.got_ip.ip),
            IP2STR(&evt->event_info.got_ip.mask),
            IP2STR(&evt->event_info.got_ip.gw));
        os_printf("\n");
        break;
    case EVENT_SOFTAPMODE_STACONNECTED:
		uart0_sendStr("EVENT_SOFTAPMODE_STACONNECTED\n");
        os_printf("station: " MACSTR "join, AID = %d\n",
            MAC2STR(evt->event_info.sta_connected.mac),
            evt->event_info.sta_connected.aid);
        break;
    case EVENT_SOFTAPMODE_STADISCONNECTED:
		uart0_sendStr("EVENT_SOFTAPMODE_STADISCONNECTED\n");
        os_printf("station: " MACSTR "leave, AID = %d\n",
            MAC2STR(evt->event_info.sta_disconnected.mac),
            evt->event_info.sta_disconnected.aid);
        break;
	default:
        break;
     }
}

